Metadata-Version: 2.1
Name: trytond_account_stock_landed_cost
Version: 7.0.0
Summary: Tryton module for landed cost
Home-page: http://www.tryton.org/
Download-URL: http://downloads.tryton.org/7.0/
Author: Tryton
Author-email: foundation@tryton.org
License: GPL-3
Project-URL: Bug Tracker, https://bugs.tryton.org/
Project-URL: Documentation, https://docs.tryton.org/
Project-URL: Forum, https://www.tryton.org/forum
Project-URL: Source Code, https://code.tryton.org/tryton
Keywords: tryton account stock valuation landed cost
Classifier: Development Status :: 5 - Production/Stable
Classifier: Environment :: Plugins
Classifier: Framework :: Tryton
Classifier: Intended Audience :: Developers
Classifier: Intended Audience :: Financial and Insurance Industry
Classifier: Intended Audience :: Legal Industry
Classifier: License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)
Classifier: Natural Language :: Bulgarian
Classifier: Natural Language :: Catalan
Classifier: Natural Language :: Chinese (Simplified)
Classifier: Natural Language :: Czech
Classifier: Natural Language :: Dutch
Classifier: Natural Language :: English
Classifier: Natural Language :: Finnish
Classifier: Natural Language :: French
Classifier: Natural Language :: German
Classifier: Natural Language :: Hungarian
Classifier: Natural Language :: Indonesian
Classifier: Natural Language :: Italian
Classifier: Natural Language :: Persian
Classifier: Natural Language :: Polish
Classifier: Natural Language :: Portuguese (Brazilian)
Classifier: Natural Language :: Romanian
Classifier: Natural Language :: Russian
Classifier: Natural Language :: Slovenian
Classifier: Natural Language :: Spanish
Classifier: Natural Language :: Turkish
Classifier: Natural Language :: Ukrainian
Classifier: Operating System :: OS Independent
Classifier: Programming Language :: Python :: 3
Classifier: Programming Language :: Python :: 3.8
Classifier: Programming Language :: Python :: 3.9
Classifier: Programming Language :: Python :: 3.10
Classifier: Programming Language :: Python :: 3.11
Classifier: Programming Language :: Python :: 3.12
Classifier: Programming Language :: Python :: Implementation :: CPython
Classifier: Topic :: Office/Business
Classifier: Topic :: Office/Business :: Financial :: Accounting
Requires-Python: >=3.8
License-File: LICENSE
Requires-Dist: trytond_account<7.1,>=7.0
Requires-Dist: trytond_account_invoice<7.1,>=7.0
Requires-Dist: trytond_product<7.1,>=7.0
Requires-Dist: trytond_stock<7.1,>=7.0
Requires-Dist: trytond<7.1,>=7.0
Provides-Extra: test
Requires-Dist: proteus<7.1,>=7.0; extra == "test"

Account Stock Landed Cost Module
################################

The account_stock_landed_cost module allows to allocate landed cost on
*Supplier Shipments* after their reception.

A new field is added to *Product*:

- *Landed Cost*: Only available for service, it allows such invoiced product to
  be used as a landed cost.

A new document *Landed Cost* defines how posted supplier invoice lines are
allocated to many shipments.

The allocation methods are:

    - *By Value*: The cost will be allocated according to the value of each
      line. (The value is: *Quantity* * *Unit *Price*)

Once posted, the *Landed Cost* updates the unit price of each incoming moves of
the shipments using the cost of all the invoice lines according to the
allocation method.

To update the cost price, the *Update Cost Price* wizard must be run on the
affected products.
